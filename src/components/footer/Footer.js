"use client";
import React from "react";
import Image from "next/image";
import { logofisio } from "@/assets";

export default function Footer() {
  return (
    <div className="flex w-screen flex-col justify-center bg-dark p-6">
      <Image
        src={logofisio}
        alt="Logo"
        className="mb-4 w-64 object-contain sm:w-48 md:w-48"
      />
      <div className="flex flex-col space-y-6 p-2 text-white md:flex-row md:space-x-40 md:space-y-0 md:p-8 md:pt-0">
        <div className="max-w-xs text-center">
          Dale vida a tus días con nuestra fisioterapia personalizada. ¡Salud y
          bienestar a tu alcance!
        </div>
        <div>
          <h2 className="font-bold"> Contactanos</h2>
          <br />
          <p className="font-bold">Ubicacion</p>
          <p>Neiva, Huila</p>
          <br />
          <p className="font-bold">Telefono</p>
          <p>(304) 614-9701</p>
          <br />
          <p className="font-bold">Email</p>
          <p>ft.carlosromero@gmail.com</p>
        </div>
        <div>
          <h2 className="font-bold">Redes sociales</h2>
          <br />
          <ul className="list-inside list-disc">
            <li>
              <a
                href="https://www.facebook.com/profile.php?id=100094490096865&mibextid=ZbWKwL"
                target="_blank"
                rel="noopener noreferrer"
              >
                Facebook
              </a>
            </li>
            <li>
              <a
                href="https://instagram.com/ft.carlosromero?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D"
                target="_blank"
                rel="noopener noreferrer"
              >
                Instagram
              </a>
            </li>
          </ul>
          <h2 className="text-lg font-bold">More</h2>
          <ul className="list-inside list-disc pl-4">
            <li>
              <a href="https://setasolutions.com.co/">
                Software por Seta Solutions
              </a>
            </li>
            <li>
              <a href="https://www.petpilotpro.com">
                PetPilot para tu practica veterinaria
              </a>
            </li>
            <li>
              <a href="https://www.bookkeepingbypaula.com/">
                Bookkeping By Paula
              </a>
            </li>
          </ul>
          <button
            onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
            className="mx-auto mt-4 block rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-primary hover:opacity-95 sm:mx-0"
          >
            Volver arriba
          </button>
        </div>
      </div>

      <div className="p-4 text-center text-white">
        <p className="mb-2">
          <a
            href="https://setasolutions.com.co/"
            target="_blank"
            rel="noopener noreferrer"
            className="px-2 text-sm underline transition duration-300 ease-in-out hover:rounded hover:bg-white hover:text-blue-700"
          >
            ¿Necesitas un sitio web?
          </a>
        </p>
        <p className="text-md">
          © 2023 Seta Solutions. All Rights Reserved. Privacy Policy | Terms of
          Service
        </p>
      </div>
    </div>
  );
}
