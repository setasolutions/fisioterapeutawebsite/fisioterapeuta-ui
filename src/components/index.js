import Navbar from "./navbar/Navbar";
import Footer from "./footer/Footer";
import CTA from "./cta/CTA";
import InfoSection from "./infoSection/infoSection";
import Gallery from "./gallery/gallery";

export { Navbar, Footer, CTA, InfoSection, Gallery };
