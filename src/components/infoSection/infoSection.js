import React from "react";
import Image from "next/image";

function InfoSection({ image, imagePosition, title, description }) {
  return (
    <div
      className={`flex flex-col overflow-hidden shadow-md md:flex-row ${
        imagePosition === "left" ? "md:flex-row" : "md:flex-row-reverse"
      } w-full`}
    >
      <div className="relative w-full flex-grow md:w-1/2">
        <Image className="h-full w-full object-cover" src={image} alt={title} />
      </div>
      <div className="flex w-full flex-col justify-center bg-background p-4 text-dark md:w-1/2 md:p-20">
        <h2 className="py-4 text-lg font-bold tracking-wide md:py-6 md:text-2xl">
          {title}
        </h2>
        <p className="text-sm leading-relaxed md:text-lg">{description}</p>
      </div>
    </div>
  );
}

export default InfoSection;
