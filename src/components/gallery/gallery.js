// "use client";
// import React, { useEffect, useState } from "react";
// import { Carousel } from "react-responsive-carousel";
// import "react-responsive-carousel/lib/styles/carousel.min.css";
// import Image from "next/image";
// import {
//   urumaster5,
//   urumaster6,
//   urumaster7,
//   urumaster8,
// } from "@/assets";

// const Gallery = () => {
//   const [images, setImages] = useState([
//     { src: urumaster5, alt: "sport boat 5" },
//     { src: urumaster6, alt: "sport boat 6" },
//     { src: urumaster7, alt: "sport boat 7" },
//     { src: urumaster8, alt: "sport boat 8" },
//   ]);

//   // A function to shuffle an array
//   const shuffle = (array) => {
//     for (let i = array.length - 1; i > 0; i--) {
//       const j = Math.floor(Math.random() * (i + 1));
//       [array[i], array[j]] = [array[j], array[i]];
//     }
//   };

//   // Reorganize images array every time the component renders
//   useEffect(() => {
//     shuffle(images);
//     setImages([...images]);
//   }, []);

//   return (
//     <Carousel
//       autoPlay
//       infiniteLoop
//       className="carousel"
//       style={{ padding: "0", margin: "0", border: "none" }}
//     >
//       {images.map((image, index) => (
//         <div key={index}>
//           <Image
//             src={image.src}
//             alt={image.alt}
//             width={500}
//             height={200}
//             className="object-cover"
//           />
//         </div>
//       ))}
//     </Carousel>
//   );
// };

// export default Gallery;
"use client";
import React, { useEffect, useState } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Image from "next/image";

const Gallery = ({ imageList }) => {
  const [images, setImages] = useState(imageList);

  // A function to shuffle an array
  const shuffle = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  };

  // Reorganize images array every time the component renders
  useEffect(() => {
    shuffle(images);
    setImages([...images]);
  }, []);

  return (
    <Carousel
      autoPlay
      infiniteLoop
      className="carousel"
      style={{ padding: "0", margin: "0", border: "none" }}
    >
      {images.map((image, index) => (
        <div key={index} className="">
          <Image
            src={image.src}
            alt={image.alt}
            width={500}
            height={200}
            className="object-cover"
          />
        </div>
      ))}
    </Carousel>
  );
};

export default Gallery;
