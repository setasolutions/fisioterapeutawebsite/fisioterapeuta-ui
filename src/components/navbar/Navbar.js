"use client";
import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { logofisio, logoBlack } from "../../assets";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faFacebookF, faInstagram } from "@fortawesome/free-brands-svg-icons";

export default function Navbar() {
  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const isScroll = window.scrollY > 50;
      if (isScroll !== isScrolled) setIsScrolled(isScroll);
    };

    document.addEventListener("scroll", handleScroll);
    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, [isScrolled]);

  return (
    <>
      <div className="fixed left-0 right-0 top-0 z-40 ">
        <div
          style={{
            backgroundColor: isScrolled
              ? "var(--dark-color)"
              : "var(--highlight-color)",
            padding: isScrolled ? "12px" : "24px",
          }}
          className="transition-all duration-500"
        >
          <div className="social-media flex items-center justify-start space-x-4 text-white">
            <a href="tel:304 6149701" className="flex items-center space-x-2">
              <FontAwesomeIcon icon={faPhone} className="text-white" />
              <span className="hidden text-white md:inline">304 6149701</span>
            </a>
            <a
              href="mailto:ft.carlosromero@gmail.com?subject=Transforme%20Su%20Salud%20con%20Nuestros%20Servicios%20de%20Fisioterapia&body=Hola,%0D%0A%0D%0AEstoy%20interesado%20en%20sus%20servicios%20de%20fisioterapia.%20%C2%BFPodr%C3%ADa%20proporcionarme%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20proceso,%20el%20tiempo%20de%20tratamiento%20y%20los%20precios?%0D%0A%0D%0AGracias,%0D%0A[Su%20Nombre]"
              className="flex items-center space-x-2"
            >
              <FontAwesomeIcon icon={faEnvelope} className="text-white" />
              <span className="text-white">ft.carlosromero@gmail.com</span>
            </a>

            <a
              href="https://www.facebook.com/profile.php?id=100094490096865&mibextid=ZbWKwL"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faFacebookF} className="text-white" />
            </a>
            <a
              href="https://www.instagram.com/ft.carlosromero/?utm_source=qr&igshid=MzNlNGNkZWQ4Mg%3D%3D"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faInstagram} className="text-white" />
            </a>
          </div>
        </div>
        <nav
          style={{ backgroundColor: "#FDFEFF" }}
          className="flex h-32 w-full flex-row items-center justify-center border-b-8 border-highlight px-4 sm:px-10 md:justify-between md:px-20"
        >
          <Link href="/">
            <Image
              src={logoBlack}
              alt="Logo"
              className="mx-auto contrast-150 drop-shadow-2xl saturate-200 filter"
              height={200}
              width={200}
            />
          </Link>
          {/* <Image
            src={title}
            alt="title"
            className="mx-auto md:mx-0 filter saturate-200 contrast-150 drop-shadow-2xl "
            height={300}
            width={300}
          /> */}

          <div className="hidden flex-col justify-center md:flex">
            <a
              href="mailto:ft.carlosromero@gmail.com?subject=Transforme%20Su%20Salud%20con%20Nuestros%20Servicios%20de%20Fisioterapia&body=Hola,%0D%0A%0D%0AEstoy%20interesado%20en%20sus%20servicios%20de%20fisioterapia.%20%C2%BFPodr%C3%ADa%20proporcionarme%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20proceso,%20el%20tiempo%20de%20tratamiento%20y%20los%20precios?%0D%0A%0D%0AGracias,%0D%0A[Su%20Nombre]"
              className="flex items-center space-x-2"
            >
              <button className="mb-4 rounded bg-highlight px-4 py-2 text-white shadow-lg transition-shadow duration-300 hover:shadow-xl">
                COTIZA EN LINEA
              </button>
            </a>
            <a
              href="tel:3046149701"
              className="inline-block rounded bg-background px-4 py-2 text-center text-highlight shadow-lg transition-shadow duration-300 hover:shadow-xl"
            >
              LLAMA 3046149701
            </a>
          </div>
        </nav>
      </div>
    </>
  );
}
