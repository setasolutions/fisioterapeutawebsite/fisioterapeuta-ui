import React from "react";
import Image from "next/image";

function CTA({
  image,
  imagePosition,
  title,
  description,
  buttonText,
  onButtonClick,
}) {
  return (
    <div className={"w-full p-4 md:p-10"}>
      <div
        className={`flex flex-col overflow-hidden rounded-lg shadow-md md:flex-row ${
          imagePosition === "left" ? "md:flex-row" : "md:flex-row-reverse"
        } w-full`}
      >
        <div className="relative w-full flex-grow md:w-1/2">
          <Image
            className="h-full w-full object-cover hover:scale-105"
            src={image}
            alt={title}
          />
        </div>

        <div className="flex w-full flex-col bg-dark p-4 text-white md:w-1/2 md:p-20">
          <h2 className="py-4 text-xl font-bold tracking-wide md:py-6 md:text-2xl">
            {title}
          </h2>
          <p className="text-sm leading-relaxed md:text-lg">{description}</p>
          <button
            onClick={onButtonClick}
            className="mt-4 self-start rounded bg-primary px-4 py-2 font-bold text-white shadow-lg hover:bg-primary hover:opacity-95"
          >
            {buttonText}
          </button>
        </div>
      </div>
    </div>
  );
}

export default CTA;
