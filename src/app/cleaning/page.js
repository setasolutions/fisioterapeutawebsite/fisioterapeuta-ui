/* eslint-disable max-len */
"use client";
import React from "react";
import Image from "next/image";
import {
  fisio2,
  fisio3,
  fisio12,
  fisio13,
  fisio19,
  fisio20,
  cleaning1,
} from "@/assets";
import { CTA, InfoSection } from "@/components";

export default function Page() {
  const handleButtonClick = () => {
    const emailSubject = encodeURIComponent(
      "Interesado en los Servicios de Fisioterapia"
    );
    const emailBody = encodeURIComponent(
      "Hola,\n\nEstoy interesado en sus servicios de fisioterapia. ¿Podría proporcionarme más información sobre el proceso, horarios disponibles y precios?\n\nGracias,\n[Su nombre]"
    );

    window.location.href = `mailto:info@fisiocarlosromero.com?subject=${emailSubject}&body=${emailBody}`;
  };

  return (
    <>
      <div className="relative mt-32 min-h-screen w-screen">
        <div className="absolute left-0 top-0 h-full w-screen bg-black opacity-75" />
        <Image
          src={fisio12}
          alt="fisio3"
          layout="fill"
          objectFit="cover"
          quality={100}
          className="z-0 brightness-75 filter"
        />

        <div className="absolute bottom-1/3 left-1/2 -translate-x-1/2 transform p-4 sm:bottom-1/4 sm:left-36 sm:translate-x-0 sm:p-10">
          <div className="z-1 text-center sm:text-left">
            <h1 className="text-2xl font-black text-white sm:text-5xl">
              ¡Tu Salud en Manos Expertas!
            </h1>
            <h2 className="mt-2 text-2xl font-black text-white sm:text-4xl">
              Carlos Eduardo Romero Castañeda, Fisioterapeuta
            </h2>
            <p className="mt-4 text-sm font-extrabold text-white sm:text-2xl">
              Especializado en atención personalizada para tus necesidades.{" "}
              <br />
              Desde afecciones crónicas hasta rehabilitación deportiva, <br />
              estoy aquí para ayudarte a recuperarte y fortalecerte. <br />
              ¡Comienza tu camino hacia una vida más saludable hoy!
            </p>

            <div className="mt-4 flex justify-center space-x-4 sm:justify-start">
              {/* <button
                className="rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-dark"
                onClick={() => router.push("/portfolio")}
              >
                View Gallery
              </button> */}
              <a href="mailto:ft.carlosromero@gmail.com?subject=Transforme%20Su%20Salud%20con%20Nuestros%20Servicios%20de%20Fisioterapia&body=Hola,%0D%0A%0D%0AEstoy%20interesado%20en%20sus%20servicios%20de%20fisioterapia.%20%C2%BFPodr%C3%ADa%20proporcionarme%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20proceso,%20el%20tiempo%20de%20tratamiento%20y%20los%20precios?%0D%0A%0D%0AGracias,%0D%0A[Su%20Nombre]">
                <button className="rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-dark">
                  Cotiza ahora
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div id="adultos-mayores">
          <CTA
            image={fisio20}
            imagePosition="left"
            title="Atención especializada para adultos mayores"
            description={
              <>
                Artritis, artrosis, osteoporosis...
                <br />
                <br /> ¡No permitas que el dolor te limite! Nuestra fisioterapia
                te ayuda a reducir el dolor, mejorar la movilidad articular y
                retrasar la evolución de estas condiciones, para que disfrutes
                cada día al máximo.
              </>
            }
            buttonText="Programa tu cita"
            onButtonClick={handleButtonClick}
          />
        </div>
        <div id="neurologia">
          <InfoSection
            image={fisio13}
            imagePosition="right"
            title="Afecciones neurológicas bajo control"
            description={
              <>
                Parkinson y Alzheimer pueden ser desafiantes, pero con nuestro
                enfoque en fisioterapia, mejorarás tu condición física, ganarás
                independencia en tus actividades básicas y minimizarás la
                progresión de la enfermedad.
              </>
            }
          />
        </div>

        <div id="condicionamiento">
          <InfoSection
            image={fisio2}
            imagePosition="left"
            title="Acondicionamiento físico y prevención de lesiones"
            description={
              <>
                No esperes a que aparezca el dolor, ¡fortalece tu cuerpo y
                prevenga las lesiones! Te ofrecemos programas personalizados de
                acondicionamiento físico que se adaptan a tus necesidades y
                metas.
              </>
            }
          />
        </div>
      </div>

      <div id="rehabilitacion">
        <CTA
          image={fisio3}
          imagePosition="right"
          title="Rehabilitación de lesiones"
          description={
            <>
              Si sufres de lesiones en músculos, tendones o ligamentos, no te
              preocupes, te ayudamos a recuperarte! Con nuestro enfoque
              integral, volverás a la actividad física de manera segura y
              efectiva.
            </>
          }
          buttonText="Cotiza ahora"
          onButtonClick={handleButtonClick}
        />
      </div>
      <div id="oncologia">
        <CTA
          image={fisio19}
          imagePosition="left"
          title="Cuidado en patologías oncológicas"
          description={
            <>
              Comprendemos los desafíos que enfrentas. Nuestra fisioterapia te
              acompañará para minimizar dolores crónicos y debilidad
              generalizada, brindándote el apoyo necesario en tu proceso de
              recuperación.
            </>
          }
          buttonText="Programa tu cita"
          onButtonClick={handleButtonClick}
        />
      </div>
    </>
  );
}
