import React from "react";
import Image from "next/image";
import { fisio6 } from "../../../assets";
import { useRouter } from "next/navigation";

export default function Hero() {
  const router = useRouter();
  return (
    <>
      <div className="relative mt-32 min-h-screen w-screen">
        <div className="absolute left-0 top-0 h-full w-screen bg-black opacity-75" />
        <Image
          src={fisio6}
          alt="fisio3"
          layout="fill"
          objectFit="cover"
          quality={100}
          className="z-0 brightness-75 filter"
        />

        <div className="absolute bottom-1/3 left-1/2 -translate-x-1/2 transform p-4 sm:bottom-1/4 sm:left-1/2 sm:p-10">
          <div className="z-1 justify-left flex flex-col text-center ">
            <h1 className="text-2xl font-black text-white sm:text-6xl">
              Fisioterapia
            </h1>
            <h1 className="text-2xl font-black text-white sm:text-3xl">
              Descubre un mundo de bienestar y vitalidad con nuestros servicios
              de fisioterapia
            </h1>
            {/* <p className="text-sm font-extrabold text-white sm:text-xl">
              Sintonízate con una salud transformadora
            </p> */}
            <button
              className="mx-auto mt-4 block rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-dark sm:mx-0"
              onClick={() => router.push("/#services")}
            >
              Conoce más
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
