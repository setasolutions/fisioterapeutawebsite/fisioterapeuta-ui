/* eslint-disable max-len */
"use client";
import React, { useState } from "react";
import Hero from "./hero/hero";
import Image from "next/image";
import {
  carlosromero2,
  fisio4,
  fisio7,
  fisio5,
  fisio16,
  fisio17,
  logofisio,
} from "@/assets";

import "./page.css";
import Card from "./card/card";
import { InfoSection } from "@/components";

export default function Page() {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <>
      <Hero />
      {/* Services */}
      <div className="w-screen p-20" id="services">
        <h1 className="text-blue mb-4 text-center text-3xl font-bold drop-shadow">
          Intervenciones de fisioterapia cuidadosamente orientadas hacia tus
          necesidades particulares
        </h1>
        <div className="flex flex-col items-center p-4">
          <div className="flex flex-wrap justify-center gap-2 sm:gap-4 md:gap-8">
            <Card
              imageSrc={fisio4}
              imageAlt="Specialized Care for Elderly People"
              description="👵👴 ATENCIÓN ESPECIALIZADA PARA ADULTOS MAYORES"
              link="/cleaning#adultos-mayores"
              className="w-full p-2 sm:w-1/2 md:w-1/3"
            />
            <Card
              imageSrc={fisio5}
              imageAlt="Neurological Disorders Treatment"
              description="🧠 AFECCIONES NEUROLÓGICAS BAJO CONTROL"
              link="/cleaning#neurologia"
              className="w-full p-2 sm:w-1/2 md:w-1/3"
            />
            <Card
              imageSrc={fisio16}
              imageAlt="Physical Conditioning and Injury Prevention"
              description="💪 ACONDICIONAMIENTO FÍSICO Y PREVENCIÓN DE LESIONES"
              link="/cleaning#condicionamiento"
              className="w-full p-2 sm:w-1/2 md:w-1/3"
            />
            <Card
              imageSrc={fisio17}
              imageAlt="Injury Rehabilitation"
              description="🤕 REHABILITACIÓN DE LESIONES"
              link="/cleaning#rehabilitacion"
              className="w-full p-2 sm:w-1/2 md:w-1/3"
            />
            <Card
              imageSrc={fisio7}
              imageAlt="Oncological Pathology Care"
              description="🌟 CUIDADO EN PATOLOGÍAS ONCOLÓGICAS"
              link="/cleaning#oncologia"
              className="w-full p-2 sm:w-1/2 md:w-1/3"
            />
          </div>
        </div>

        <div
          className="slide-bottom flex justify-center p-10"
          onMouseEnter={() => setIsHovered(true)}
          onMouseLeave={() => setIsHovered(false)}
        >
          <Image
            src={logofisio}
            alt="Logo"
            className={`w-64 object-contain sm:w-48 md:w-48 ${
              isHovered ? "slide-bottom" : ""
            }`}
          />
        </div>
      </div>
      <div
        id="entry"
        className="w-screen "
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <div className="flex flex-col items-center justify-center p-10">
          <div className="max-w-6xl px-6 py-2 sm:px-10 md:px-20 lg:px-48">
            <h1 className="mb-4 text-center text-2xl font-bold text-dark drop-shadow sm:text-3xl">
              FISIOTERAPIA PERSONALIZADA:
            </h1>
            <p className="mb-6 text-center text-sm font-bold text-dark sm:text-base md:text-lg">
              Cada tratamiento se ajusta a tu perfil específico con técnicas
              eficaces para garantizar un cuidado óptimo para potenciar tu
              capacidad de movimiento, tu fortaleza y tu calidad de vida,
              permitiéndote retomar tus actividades diarias con confianza y
              plenitud.
            </p>
            <a
              href="mailto:ft.carlosromero@gmail.com?subject=Transforme%20Su%20Salud%20con%20Nuestros%20Servicios%20de%20Fisioterapia&body=Hola,%0D%0A%0D%0AEstoy%20interesado%20en%20sus%20servicios%20de%20fisioterapia.%20%C2%BFPodr%C3%ADa%20proporcionarme%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20proceso,%20el%20tiempo%20de%20tratamiento%20y%20los%20precios?%0D%0A%0D%0AGracias,%0D%0A[Su%20Nombre]"
              className="flex items-center space-x-2"
            >
              <button
                className={`mx-auto mt-4 block rounded bg-primary px-6 py-3 font-bold text-white shadow-lg transition duration-500 ease-in-out hover:-translate-y-1 hover:scale-110 hover:bg-dark ${
                  isHovered ? "slide-in-right" : ""
                }`}
              >
                Cotiza en línea
              </button>
            </a>
          </div>
        </div>
        <InfoSection
          image={carlosromero2}
          imagePosition="left"
          title="Carlos Eduardo Romero Castañeda: Tu Terapeuta de Fisioterapia 🎓"
          description={
            <>
              Soy Carlos Eduardo Romero Castañeda, graduado en fisioterapia de
              la Fundación Universitaria María Cano.
              <br />
              <br />
              Mi pasión es mejorar la calidad de vida de mis pacientes a través
              de un enfoque personalizado y compasivo. Con experiencia en una
              variedad de tratamientos y técnicas, estoy listo para ayudarte a
              alcanzar tus objetivos de bienestar y salud.
              <br />
              <br />
              Si buscas un terapeuta comprometido y experimentado, ¡contáctame
              hoy! Juntos, podemos trabajar para vivir una vida más activa y
              saludable. 🌟
            </>
          }
        />
      </div>

      {/* CTA */}
      <div className=" p-16">
        <div className="flex flex-col items-center justify-between rounded-3xl bg-dark p-5 shadow-2xl sm:flex-row sm:space-x-4 sm:space-y-0">
          <div className="text-center text-lg font-bold text-white sm:mb-0">
            Tu bienestar es nuestra prioridad! No esperes más para mejorar tu
            calidad de vida.
            <br />
            Contáctanos ahora y comienza a vivir una vida activa y saludable!
            🌈💫
          </div>
          <a href="tel:3046149701">
            <button className="text-md rounded bg-primary px-4 py-2 font-bold text-white">
              Llama ahora
            </button>
          </a>
        </div>
      </div>
    </>
  );
}
