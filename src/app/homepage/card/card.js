"use client";
import React, { useState } from "react";
import Image from "next/image";
import "./style.css";
import { useRouter } from "next/navigation";

function Card({ imageSrc, imageAlt, title, description, link }) {
  const router = useRouter();
  const [isHovered, setIsHovered] = useState(false);

  return (
    <div
      className={`m-6 flex w-[350px] flex-col items-center space-y-2 rounded-lg bg-dark p-8 shadow-xl ${
        isHovered ? "shadow-pop-tr" : ""
      }`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div className="relative flex h-44 w-40 flex-col items-center justify-center">
        <Image
          src={imageSrc}
          alt={imageAlt}
          className="z-10 h-32 w-full rounded object-cover transition-transform hover:scale-110"
        />
        <h2 className="absolute bottom-2 left-0 right-0 z-20 text-center text-sm font-bold text-dark">
          {title}
        </h2>
      </div>

      <br />

      <div className="max-w-sm">
        <p className="text-center text-sm font-bold text-white">
          {description}
        </p>
      </div>
      <button
        className="mx-auto mt-4 block rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-primary hover:opacity-95 sm:mx-0"
        onClick={() => router.push(link)}
      >
        Leer más
      </button>
    </div>
  );
}

export default Card;
