"use client";
import React from "react";
import Image from "next/image";
import {
  urumaster6,
  BA1,
  BA2,
  BA3,
  BA6,
  ig1b1,
  ig1b2,
  ig2b1,
  ig2b2,
  ig2b3,
  ig3b1,
  ig3b2,
  ig3b3,
  ig6b1,
  ig6b2,
  ig7b1,
  ig7b2,
  ig8b1,
  ig8b2,
  more1,
} from "@/assets";
import { CTA, InfoSection, Gallery } from "@/components";

export default function Page() {
  const imageList = [
    { src: BA1, alt: "sport boat 54" },
    { src: BA2, alt: "sport boat 64" },
    { src: BA3, alt: "sport boat 73" },
    { src: BA6, alt: "sport boat 83" },
  ];

  const imageList1 = [
    { src: ig1b1, alt: "Sport Boat 1" },
    { src: ig1b2, alt: "Sport Boat 2" },
  ];

  const imageList2 = [
    { src: ig2b1, alt: "Sport Boat 3" },
    { src: ig2b2, alt: "Sport Boat 4" },
    { src: ig2b3, alt: "Sport Boat 5" },
  ];

  const imageList3 = [
    { src: ig3b1, alt: "Sport Boat 6" },
    { src: ig3b2, alt: "Sport Boat 7" },
    { src: ig3b3, alt: "Sport Boat 8" },
  ];

  const imageList6 = [
    { src: ig6b1, alt: "Sport Boat 9" },
    { src: ig6b2, alt: "Sport Boat 10" },
  ];

  const imageList7 = [
    { src: ig7b1, alt: "Sport Boat 11" },
    { src: ig7b2, alt: "Sport Boat 12" },
  ];

  const imageList8 = [
    { src: ig8b1, alt: "Sport Boat 13" },
    { src: ig8b2, alt: "Sport Boat 14" },
  ];

  const handleButtonClick = () => {
    const emailSubject = encodeURIComponent(
      "Transform Your Boat with Our Restoration and Cleaning Services"
    );
    const emailBody = encodeURIComponent(
      "Hello,\n\nI am interested in your services. Could you please provide me with more information about the process, timeline, and pricing?\n\nThank you,\n[Your Name]"
    );

    const emailAddress = "urumaster@boatsrestoration.com";
    const mailtoLink = `mailto:${emailAddress}?subject=${emailSubject}&body=${emailBody}`;
    window.location.href = mailtoLink;
  };

  return (
    <>
      <div className="relative mt-32 min-h-screen w-screen">
        <div className="absolute left-0 top-0 h-full w-screen bg-black opacity-75" />

        <Image
          src={urumaster6}
          imageList3
          alt="fisio3"
          layout="fill"
          objectFit="cover"
          quality={100}
          className="z-0 brightness-75 filter"
        />

        <div className="absolute bottom-1/3 left-1/2 -translate-x-1/2 transform p-4 sm:bottom-1/4 sm:left-36 sm:translate-x-0 sm:p-10">
          <div className="z-1 text-center sm:text-left">
            <h1 className="text-2xl font-black text-white sm:text-6xl">
              Unparalleled Attention to Your Vessel
            </h1>
            <h2 className="mt-2 text-2xl font-black text-white sm:text-4xl">
              Leaders in Nautical Maintenance
            </h2>
            <p className="mt-4 text-sm font-extrabold text-white sm:text-2xl">
              Our mission is to keep your vessel primed and ready, prepped for
              your next aquatic journey. Consider us as your watercraft&apos;s
              loyal companion, ensuring it&apos;s always pristine and primed for
              your upcoming expeditions.
            </p>
          </div>
        </div>
      </div>

      <div>
        <h1 className="m-8 p-8 text-center text-2xl font-extrabold text-white drop-shadow-2xl sm:text-4xl lg:text-6xl">
          Gallery
        </h1>

        <div className="flex flex-wrap justify-around">
          <div className="w-full p-4 sm:w-1/2 lg:w-1/3">
            <Gallery title="Gallery 1" imageList={imageList1} />
          </div>
          <div className="w-full p-4 sm:w-1/2 lg:w-1/3">
            <Gallery title="Gallery 2" imageList={imageList2} />
          </div>
          <div className="w-full p-4 sm:w-1/2 lg:w-1/3">
            <Gallery title="Gallery 3" imageList={imageList3} />
          </div>
          <div className="w-full p-4 sm:w-1/2 lg:w-1/3">
            <Gallery title="Gallery 4" imageList={imageList6} />
          </div>
          <div className="w-full p-4 sm:w-1/2 lg:w-1/3">
            <Gallery title="Gallery 5" imageList={imageList7} />
          </div>
          <div className="w-full p-4 sm:w-1/2 lg:w-1/3">
            <Gallery title="Gallery 6" imageList={imageList8} />
          </div>
        </div>

        <CTA
          image={more1}
          imagePosition="left"
          title="PREMIUM CARE FOR YOUR BOAT"
          description="Experience exceptional boat maintenance services tailored to your needs. Our dedicated team ensures your watercraft is always ready for the water, offering comprehensive care and meticulous detailing."
          buttonText="Explore Now"
          onButtonClick={handleButtonClick}
        />
      </div>
    </>
  );
}
