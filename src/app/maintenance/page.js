/* eslint-disable max-len */
"use client";
import React from "react";
import Image from "next/image";
import {
  urumaster4,
  urumaster5,
  urumaster9,
  maintenance1,
  maintenance2,
  maintenance3,
  maintenance4,
  maintenance5,
  BA1,
  BA2,
  BA3,
  BA4,
  BA6,
} from "@/assets";
import { CTA, InfoSection, Gallery } from "@/components";
import { useRouter } from "next/navigation";

export default function Page() {
  const router = useRouter();
  const imageList = [
    { src: BA1, alt: "sport boat 54" },
    { src: BA2, alt: "sport boat 64" },
    { src: BA3, alt: "sport boat 73" },
    { src: BA4, alt: "sport boat 74" },
    { src: BA6, alt: "sport boat 83" },
  ];

  const handleButtonClick = () => {
    const emailSubject = encodeURIComponent(
      "Transform Your Boat with Our Restoration and Cleaning Services"
    );
    const emailBody = encodeURIComponent(
      "Hello,\n\nI am interested in your services. Could you please provide me with more information about the process, timeline, and pricing?\n\nThank you,\n[Your Name]"
    );

    window.location.href = `mailto:urumaster@boatsrestoration.com?subject=${emailSubject}&body=${emailBody}`;
  };

  return (
    <>
      <div className="relative mt-32 min-h-screen w-screen">
        <div className="absolute left-0 top-0 h-full w-screen bg-black opacity-75" />
        <Image
          src={urumaster4}
          alt="fisio3"
          layout="fill"
          objectFit="cover"
          quality={100}
          className="z-0 brightness-75 filter"
        />

        <div className="absolute bottom-1/3 left-1/2 -translate-x-1/2 transform p-4 sm:bottom-1/4 sm:left-36 sm:translate-x-0 sm:p-10">
          <div className="z-1 text-center sm:text-left">
            <h1 className="text-2xl font-black text-white sm:text-5xl">
              Unmatched Maintenance for Your Vessel
            </h1>
            <h2 className="mt-2 text-2xl font-black text-white sm:text-4xl">
              Expert Marine Maintenance Services, Buford, GA
            </h2>
            <p className="mt-4 text-sm font-extrabold text-white sm:text-2xl">
              We&apos;re dedicated to ensuring your boat&apos;s performance
              never falters, keeping it primed for the open waters. Consider us
              your watercraft&apos;s trusted guardian, ensuring its continual
              readiness for your future voyages.
            </p>
            <div className="mt-4 flex justify-center space-x-4 sm:justify-start">
              <button
                className="rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-dark"
                onClick={() => router.push("/portfolio")}
              >
                View Gallery
              </button>
              <a href="mailto:urumaster@boatsrestoration.com?subject=Transform%20Your%20Boat%20with%20Our%20Restoration%20and%20Cleaning%20Services&body=Hello,%0D%0A%0D%0AI am interested in your services. Could you please provide me with more information about the process, timeline, and pricing?%0D%0A%0D%0AThank you,%0D%0A[Your Name]">
                <button className="rounded bg-primary px-6 py-3 font-bold text-white shadow-lg hover:bg-dark">
                  Book Now
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div>
        <InfoSection
          image={maintenance1}
          imagePosition="right"
          title="Crafting Perfection: 15+ Years of Fiberglass and Gel Coat Mastery"
          description="Good fiberglass and gel coat repair people did not start yesterday. It is an art that requires many years of dedication, time and patience. Our consistent high quality work is performed with over 15 years of experience."
          buttonText="BOOK NOW"
          onButtonClick={handleButtonClick}
        />
        <InfoSection
          image={maintenance2}
          imagePosition="left"
          title="Restoring Brilliance: Expert Repairs for Gel Coat and Fiberglass Damage"
          description="Our trained experts repair minor and major gel coat and fiberglass damage including holes, cracks, chips, air pockets, scrapes, gashes, spidering or “crazing”, osmotic blistering a.ka. “boat pox” scratches, dock rash and delaminations.
                    If you boat has any type of fiberglass impact or structural damage, fractures, soft or rotted floor, transoms and/or stringer, we can help."
          buttonText="BOOK NOW"
          onButtonClick={handleButtonClick}
        />
      </div>
      <div className="relative flex h-screen items-center justify-center p-4">
        <div className="max-h-lg z-10 mb-4 max-w-xl">
          <Gallery imageList={imageList} />
        </div>
        <div className="absolute inset-x-0 top-0 z-0 h-1/2 w-screen">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#f4fbfd"
              fill-opacity="1"
              d="M0,128L8.3,128C16.6,128,33,128,50,154.7C66.2,181,83,235,99,266.7C115.9,299,132,309,149,266.7C165.5,224,182,128,199,85.3C215.2,43,232,53,248,64C264.8,75,281,85,298,122.7C314.5,160,331,224,348,213.3C364.1,203,381,117,397,106.7C413.8,96,430,160,447,192C463.4,224,480,224,497,234.7C513.1,245,530,267,546,261.3C562.8,256,579,224,596,192C612.4,160,629,128,646,144C662.1,160,679,224,695,213.3C711.7,203,728,117,745,74.7C761.4,32,778,32,794,64C811,96,828,160,844,160C860.7,160,877,96,894,69.3C910.3,43,927,53,943,90.7C960,128,977,192,993,224C1009.7,256,1026,256,1043,240C1059.3,224,1076,192,1092,160C1109,128,1126,96,1142,101.3C1158.6,107,1175,149,1192,160C1208.3,171,1225,149,1241,149.3C1257.9,149,1274,171,1291,181.3C1307.6,192,1324,192,1341,176C1357.2,160,1374,128,1390,144C1406.9,160,1423,224,1432,256L1440,288L1440,0L1431.7,0C1423.4,0,1407,0,1390,0C1373.8,0,1357,0,1341,0C1324.1,0,1308,0,1291,0C1274.5,0,1258,0,1241,0C1224.8,0,1208,0,1192,0C1175.2,0,1159,0,1142,0C1125.5,0,1109,0,1092,0C1075.9,0,1059,0,1043,0C1026.2,0,1010,0,993,0C976.6,0,960,0,943,0C926.9,0,910,0,894,0C877.2,0,861,0,844,0C827.6,0,811,0,794,0C777.9,0,761,0,745,0C728.3,0,712,0,695,0C678.6,0,662,0,646,0C629,0,612,0,596,0C579.3,0,563,0,546,0C529.7,0,513,0,497,0C480,0,463,0,447,0C430.3,0,414,0,397,0C380.7,0,364,0,348,0C331,0,314,0,298,0C281.4,0,265,0,248,0C231.7,0,215,0,199,0C182.1,0,166,0,149,0C132.4,0,116,0,99,0C82.8,0,66,0,50,0C33.1,0,17,0,8,0L0,0Z"
            ></path>
          </svg>
        </div>
      </div>

      <div id="interior-maintenance">
        <CTA
          image={maintenance3}
          imagePosition="left"
          title="Interior Maintenance"
          description={
            <>
              - Regular interior wash-down and upkeep.
              <br />
              - Isinglass treatment for clear and protected surfaces.
              <br />
              - Comprehensive carpet cleaning for indoor and outdoor spaces.
              <br />
              - High-speed buffing to maintain the polished look.
              <br />- Mildew prevention and treatment.
            </>
          }
          buttonText="SCHEDULE MAINTENANCE"
          onButtonClick={handleButtonClick}
        />
      </div>
      <div id="repairs">
        <CTA
          image={maintenance5}
          imagePosition="right"
          title="Boat Repair Services"
          description={
            <>
              - Expert fiberglass and gel coat repairs.
              <br />
              - Professional bottom paint application.
              <br />
              - Precise decal placements for a customized look.
              <br />
              - Comprehensive polish, compound, and wax services.
              <br />- Structural repairs and leak fixes.
            </>
          }
          buttonText="SCHEDULE REPAIR"
          onButtonClick={handleButtonClick}
        />
      </div>
      <CTA
        image={maintenance4}
        imagePosition="left"
        title="Exterior Repair Services"
        description={
          <>
            - Specialized fiberglass and gel coat repairs.
            <br />
            - High-quality bottom paint application.
            <br />
            - Detailed decal placements for personalization.
            <br />
            - Thorough polish, compound, and wax for a lasting shine.
            <br />- Hull and deck repair services.
          </>
        }
        buttonText="SCHEDULE REPAIR"
        onButtonClick={handleButtonClick}
      />
    </>
  );
}
