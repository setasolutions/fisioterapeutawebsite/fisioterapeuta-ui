import logoBlack from "./logoBlack.png";
import logoWhite from "./logoWhite.png";

import logo from "./logourumaster.svg";
import heroImage from "./heroImage.jpg";
import menu from "./burgerMenu.svg";
import logoBlanco from "./logopatoblanco.png";
import title from "./title.svg";

import URUMASTER from "./URUMASTER.svg";
import urumaster4 from "./urumaster4.png";
import urumaster5 from "./urumaster5.png";
import urumaster6 from "./urumaster6.jpg";
import urumaster7 from "./urumaster7.jpg";
import urumaster8 from "./urumaster8.jpg";
import urumaster9 from "./urumaster9.jpg";

import service1 from "./service1.png";
import service2 from "./service2.png";
import service3 from "./service3.png";
import service4 from "./service4.png";
import service5 from "./service5.png";
import service6 from "./service6.jpg";
import service7 from "./service7.jpg";

import maintenance1 from "./maintenance1.jpeg";
import maintenance2 from "./maintenance2.jpeg";
import maintenance3 from "./maintenance3.jpeg";
import maintenance4 from "./maintenance4.jpeg";
import maintenance5 from "./maintenance5.jpeg";

import cleaning1 from "./cleaning1.jpeg";

import BA1 from "./before-after-1.jpg";
import BA2 from "./before-after-2.jpg";
import BA3 from "./before-after-3.jpg";
import BA4 from "./before-after-4.jpg";
import BA5 from "./before-after-5.jpg";
import BA6 from "./before-after-6.jpg";
import BA7 from "./before-after-7.jpg";

import ig1b1 from "./ig1b1.jpeg";
import ig1b2 from "./ig1b2.jpeg";
import ig2b1 from "./ig2b1.jpeg";
import ig2b2 from "./ig2b2.jpeg";
import ig2b3 from "./ig2b3.jpeg";
import ig3b1 from "./ig3b1.jpeg";
import ig3b2 from "./ig3b2.jpeg";
import ig3b3 from "./ig3b3.jpeg";

import ig6b1 from "./ig6b1.jpeg";
import ig6b2 from "./ig6b2.jpeg";

import ig7b1 from "./ig7b1.jpeg";
import ig7b2 from "./ig7b2.jpeg";
import ig8b1 from "./ig8b1.jpeg";
import ig8b2 from "./ig8b2.jpeg";

import more1 from "./more1.jpeg";

import logofisio from "./logofisio.svg";
import fisio1 from "./fisio1.jpeg";
import fisio2 from "./fisio2.jpg";
import fisio3 from "./fisio3.jpg";
import fisio4 from "./fisio4.jpg";
import fisio5 from "./fisio5.jpg";
import fisio6 from "./fisio6.jpg";
import fisio7 from "./fisio7.jpg";
import fisio8 from "./fisio8.jpg";
import fisio9 from "./fisio9.jpg";
import fisio10 from "./fisio10.jpg";
import fisio11 from "./fisio11.jpg";
import fisio12 from "./fisio12.jpeg";
import fisio13 from "./fisio13.jpeg";
import fisio14 from "./fisio14.jpeg";
import fisio15 from "./fisio15.jpeg";
import fisio16 from "./fisio16.jpeg";
import fisio17 from "./fisio17.jpeg";
import fisio19 from "./fisio19.jpeg";
import fisio20 from "./fisio20.jpeg";

import carlosromero from "./carlosromero.png";
import carlosromero1 from "./carlosromero1.png";
import carlosromero2 from "./carlosromero2.jpeg";
export {
  logoBlack,
  logoWhite,
  fisio1,
  fisio2,
  fisio3,
  fisio4,
  fisio5,
  fisio6,
  fisio7,
  fisio8,
  fisio9,
  fisio10,
  fisio11,
  fisio12,
  fisio13,
  fisio14,
  fisio15,
  fisio16,
  fisio17,
  fisio19,
  fisio20,
  logo,
  heroImage,
  menu,
  logoBlanco,
  title,
  URUMASTER,
  urumaster4,
  urumaster5,
  urumaster6,
  urumaster7,
  urumaster8,
  urumaster9,
  service1,
  service2,
  service3,
  service4,
  service5,
  service6,
  service7,
  maintenance1,
  maintenance2,
  maintenance3,
  maintenance4,
  maintenance5,
  cleaning1,
  BA1,
  BA2,
  BA3,
  BA4,
  BA5,
  BA6,
  BA7,
  ig1b1,
  ig1b2,
  ig2b1,
  ig2b2,
  ig2b3,
  ig3b1,
  ig3b2,
  ig3b3,
  ig6b1,
  ig6b2,
  ig7b1,
  ig7b2,
  ig8b1,
  ig8b2,
  more1,
  logofisio,
  carlosromero,
  carlosromero1,
  carlosromero2,
};
