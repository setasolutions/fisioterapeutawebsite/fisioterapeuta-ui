module.exports = {
  extends: ["next/core-web-vitals"],
  rules: {
    "max-len": [2, 550],
    "no-multiple-empty-lines": [
      "warn",
      {
        max: 3,
        maxEOF: 3,
      },
    ],
    "no-unused-vars": "warn",
  },
};
